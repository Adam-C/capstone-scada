﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scada_server
{
    public class Log
    {
        private static StreamWriter GetStreamWriter(string path)
        {
            return File.AppendText(path);
        }

        public static void WriteMessage(string message)
        {
            using (var writer = GetStreamWriter(Constants.LogFilePath))
            {
                // Add date time
                message = DateTime.Now + " -- " + message;
                writer.WriteLine(message);
            }
        }
    }
}
