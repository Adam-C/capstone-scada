﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace scada_server.Logic
{
    public static class CveSearch
    {
        public static List<ActionData.Vulnerability> SearchKeyword(string keyword, bool htmlScrapeOnly)
        {
            // Create request data using keyword
            var postData = "search=" + WebUtility.HtmlEncode(keyword);
            var data = Encoding.ASCII.GetBytes(postData);

            var domain = "https://cve.circl.lu";

            var content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("search", WebUtility.HtmlEncode(keyword))
            });

            var client = new HttpClient();
            client.BaseAddress = new Uri(domain);
            var response = client.PostAsync("/search", content).Result;

            // Create response object
            List<ActionData.Vulnerability> results = new List<ActionData.Vulnerability>();

            if (response.Content != null)
            {
                // Response will be raw HTML
                var responseString = response.Content.ReadAsStringAsync().Result;

                var doc = new HtmlDocument();
                doc.LoadHtml(responseString);

                var cveTable = doc.GetElementbyId("CVEs");
                var tbody = cveTable.ChildNodes.First(n => n.Name == "tbody");

                // Add values to return collection
                foreach (var node in tbody.ChildNodes.Where(n => n.Name == "tr"))
                {
                    // Get Name
                    var name =
                        node.ChildNodes.Where(n => n.Name == "td").ElementAt(1)
                            .ChildNodes.First(n => n.Name == "a").InnerText ?? string.Empty;


                    // Use Name to get CVE from JSON endpoint
                    if (!htmlScrapeOnly)
                    {
                        var cveData = GetCveData(name);

                        var description = cveData["summary"].ToString();

                        var cvss = cveData["cvss"].ToString();

                        var referenceLinks = cveData["references"].ToObject<List<string>>();

                        // Collect values and add to return collection
                        results.Add(new ActionData.Vulnerability
                        {
                            Name = name,
                            Description = description,
                            CVSS = cvss,
                            ReferenceLinks = referenceLinks
                        });

                    }
                    // Otherwise perform manual HTML scraping
                    else
                    {
                        // Get Description
                        var description =
                            node.ChildNodes.Where(n => n.Name == "td").ElementAt(3)
                                .ChildNodes.First(n => n.Name == "div").GetAttributeValue("title", string.Empty) ?? string.Empty;

                        // Get CVSS number
                        var cvss =
                            node.ChildNodes.Where(n => n.Name == "td").ElementAt(2).InnerText ?? "0.0";
                        // Need to strip newlines and whitespace
                        cvss = Regex.Replace(cvss, @"\s+", string.Empty);

                        // Get single reference link
                        var referenceLink =
                            node.ChildNodes.Where(n => n.Name == "td").ElementAt(1)
                                .ChildNodes.First(n => n.Name == "a").GetAttributeValue("href", string.Empty) ??
                            string.Empty;

                        // Fix path
                        if (!string.IsNullOrEmpty(referenceLink))
                            referenceLink = domain + referenceLink;

                        // Collect values and add to return collection
                        results.Add(new ActionData.Vulnerability
                        {
                            Name = name,
                            Description = description,
                            CVSS = cvss,
                            ReferenceLinks = new List<string> { referenceLink }
                        });
                    }
                }
            }

            return results;
        }

        private static dynamic GetCveData(string name)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://cve.circl.lu/api/cve/");
            var response = client.GetStreamAsync(name).Result;

            if (response != null)
            {
                var cveJson = new StreamReader(response).ReadToEnd();

                dynamic cve = JsonConvert.DeserializeObject(cveJson);

                return cve;
            }

            return null;
        }
    }
}
