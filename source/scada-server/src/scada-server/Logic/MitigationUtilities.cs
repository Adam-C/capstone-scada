using System;
using System.Net.Http;
using System.Linq;
using Microsoft.Data.Sqlite;

namespace scada_server.Logic {

    public static class MitigationUtilities {

        // Scrape any known domains for mitigation text to insert into database
        public static void ScrapeLinkForMitigation(SqliteConnection conn, string vulnId, string link) {

            var uri = new Uri(link);

            // If host is known as scrapable
            if (uri.Host == "www.kb.cert.org") {

                // Retrieve page
                var client = new HttpClient();
                client.BaseAddress = uri;
                var html = client.GetAsync(string.Empty).Result.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(html)) {
                    var doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);

                    var mitigationContainer = doc.GetElementbyId("vulnerability-note-content")
                                            .ChildNodes.FirstOrDefault(n => (n.Name == "h3" && n.InnerText == "Solution"));

                    if (mitigationContainer == null) return;

                    mitigationContainer = mitigationContainer.NextSibling.NextSibling;

                    string mitigationText = string.Empty;

                    foreach (var node in mitigationContainer.ChildNodes) {
                        mitigationText += node.InnerText;
                    }

                    if (!string.IsNullOrEmpty(mitigationText.Trim())) {
                        AddMitigationToDb(conn, vulnId, mitigationText, link);
                    }
                }
                

            } else if (uri.Host == "ics-cert.us-cert.gov") {
                
                var client = new HttpClient();
                client.BaseAddress = uri;
                // Fake the user agent so a 403 Forbidden is not received
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"); 

                var response = client.GetAsync(string.Empty).Result;
                var html = response.Content.ReadAsStringAsync().Result;

                if (!string.IsNullOrEmpty(html)) {
                    var doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(html);

                    var mitigationHeader = doc.GetElementbyId("ncas-content");
                    if (mitigationHeader != null) {
                        mitigationHeader = mitigationHeader.Descendants().FirstOrDefault(n => n.InnerText == "MITIGATION");

                        if (mitigationHeader != null) {
                            string mitigationText = string.Empty;
                            HtmlAgilityPack.HtmlNode current = mitigationHeader;

                            while (!current.Attributes.Where(a => (a.Name == "class" && a.Value == "footnotes")).Any()) {
                                mitigationText += current.InnerText + "\r\n";
                                current = current.NextSibling;
                            }
                            
                            if (!string.IsNullOrEmpty(mitigationText.Trim())) {
                                AddMitigationToDb(conn, vulnId, mitigationText, link);
                            }
                        }
                    }                    
                }
            }
        }

        private static void AddMitigationToDb(SqliteConnection conn, string vulnId, string mitigationText, string link) {

            using (var command = conn.CreateCommand()) {

                        command.CommandText = @"INSERT INTO 'Mitigations' VALUES (
                            NULL,
                            @vulnId,
                            @mitigationText,
                            @link
                            )";

                        command.Parameters.Add(new SqliteParameter("vulnId", vulnId));
                        command.Parameters.Add(new SqliteParameter("mitigationText", mitigationText));
                        command.Parameters.Add(new SqliteParameter("link", link));

                        command.ExecuteNonQuery();
            }
            
        }
    }
}