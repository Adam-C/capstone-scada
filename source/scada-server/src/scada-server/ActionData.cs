﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scada_server
{
    /// <summary>
    /// Reduced version of ActionData found in scada-app.
    /// </summary>
    public class ActionData
    {
        public class Vulnerability
        {
            public int Id = -1;

            public string Name = string.Empty;

            public string Type = string.Empty;

            public string Description = string.Empty;

            public string CVSS = string.Empty;

            public string Severity = string.Empty;

            public List<string> ReferenceLinks = new List<string>();
        }
    }
}
