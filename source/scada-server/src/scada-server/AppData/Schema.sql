﻿BEGIN TRANSACTION;

DROP TABLE IF EXISTS "Vulnerabilities";
CREATE TABLE "Vulnerabilities" (
	"ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"Name"	TEXT,
	"Published"	TEXT,
	"Description"	TEXT,
	"CVSS"	REAL,
	"Protocol"	TEXT
);

/* This table associates multiple reference links to a vulnerability */
DROP TABLE IF EXISTS "Reference_Links";
CREATE TABLE "Reference_Links" (
	"ID"	INTEGER NOT NULL,
	"Link"	TEXT NOT NULL,
	PRIMARY KEY("ID","Link"),
	FOREIGN KEY("ID") REFERENCES Vulnerabilities(ID)
);

DROP TABLE IF EXISTS "Mitigations";
CREATE TABLE "Mitigations" (
	"ID"	INTEGER NOT NULL,
	"Vulnerability_ID"	INTEGER,
	"Description"	TEXT,
	"Reference_Link" TEXT,
	PRIMARY KEY("ID"),
	FOREIGN KEY("Vulnerability_ID") REFERENCES "Vulnerabilities"("ID")
);

DROP TABLE IF EXISTS "Database_Version";
CREATE TABLE "Database_Version" (
	"Version"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("Version")
);
INSERT INTO "Database_Version" VALUES ('v01');
COMMIT;
