﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;


namespace scada_server
{
    public class Program
    {
        public static void Main(string[] args)
        {

            // Initialise log file
            if (!File.Exists(Constants.LogFilePath))
            {
                if (!Directory.Exists(Constants.LogDirectory))
                {
                    Directory.CreateDirectory(Constants.LogDirectory);
                }
                FileStream file = File.Create(Constants.LogFilePath);
                file.Dispose();
            }

            var dbPath = Directory.GetCurrentDirectory() + Constants.DatabaseFileName;
            var schemaPath = Directory.GetCurrentDirectory() + Constants.SchemaSqlFileName;

            DbSetup.InitialiseDb(dbPath, schemaPath);
            DbUpdate.BeginUpdateProcedure(dbPath);

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
