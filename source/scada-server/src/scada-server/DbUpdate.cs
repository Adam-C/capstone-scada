﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace scada_server
{
    public static class DbUpdate
    {
        /// <summary>
        /// Creates new thread to start update procedure on.
        /// </summary>
        /// <param name="dbPath"></param>
        public static void BeginUpdateProcedure(string dbPath)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                UpdateDatabaseFromExternalSources(dbPath);

            }).Start();
        }

        /// <summary>
        /// Finds information from external sources and inserts into server database.
        /// CURRENTLY only adds test data.
        /// </summary>
        /// <param name="dbPath"></param>
        private static void UpdateDatabaseFromExternalSources(string dbPath)
        {

            var keywordList = new List<string>
            {
                "Modbus",
                "Ethernet/IP",
                "Profinet",
            };

            // TODO: could abstract this loop into another place
            // Perform lookup of https://cve.circl.lu using keyword list
            foreach (var keyword in keywordList)
            {
                foreach (var result in Logic.CveSearch.SearchKeyword(keyword, false))
                {
                    // Insert result values into database
                    using (var conn = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = dbPath }.ToString()))
                    {
                        conn.Open();

                        // Ensure we don't add duplicate entries
                        var ignoreEntry = false;
                        using (var command = conn.CreateCommand()) {
                            command.CommandText = @"SELECT Name FROM Vulnerabilities WHERE Name = @name";
                            command.Parameters.Add(new SqliteParameter("name", result.Name));
                            
                            using (var reader = command.ExecuteReader()) {
                                // Set flag to ignore this entry if there are existing with same name
                                if (reader.Read()) ignoreEntry = true;
                            }
                        }

                        if (ignoreEntry) continue;

                        using (var command = conn.CreateCommand())
                        {
                            command.CommandText =
                                @"INSERT INTO 'Vulnerabilities' VALUES (
                                NULL, 
                                @name, 
                                NULL,
                                @description,
                                @cvss,
                                @protocolKeyword);";

                            // Set parameters
                            command.Parameters.Add(new SqliteParameter("name", result.Name));
                            command.Parameters.Add(new SqliteParameter("description", result.Description));
                            command.Parameters.Add(new SqliteParameter("cvss", double.Parse(result.CVSS)));
                            command.Parameters.Add(new SqliteParameter("protocolKeyword", keyword));

                            command.ExecuteNonQuery();
                        }

                        // Add reference links (and possibly mitigations)
                        using (var command = conn.CreateCommand())
                        {
                            // Get ID of the vulnerability we just added
                            command.CommandText = @"SELECT ID FROM Vulnerabilities WHERE ROWID = last_insert_rowid()";

                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    var id = reader.GetString(0);

                                    foreach (var link in result.ReferenceLinks)
                                    {
                                        using (var commandAdd = conn.CreateCommand())
                                        {
                                            commandAdd.CommandText = @"INSERT INTO Reference_Links VALUES (@id, @link);";

                                            commandAdd.Parameters.Add(new SqliteParameter("id", id));
                                            commandAdd.Parameters.Add(new SqliteParameter("link", link));

                                            commandAdd.ExecuteNonQuery();
                                        }

                                        // Scrape the reference link for mitigations and add them
                                        Logic.MitigationUtilities.ScrapeLinkForMitigation(conn, id, link);
                                    }
                                }
                            }

                        }
                    }
                }
            }

            // TODO: update from other sources


            // Wait for a fair period of time
            Thread.Sleep(TimeSpan.FromHours(Constants.DatabaseUpdateWaitTimeHours));

            // Repeat update procedure
            UpdateDatabaseFromExternalSources(dbPath);
        }
    }
}
