﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.Sqlite;

namespace scada_server
{
    public static class DbSetup
    {
        /// <summary>
        /// Performs database initialisation on program startup. 
        /// Reset and update entire database according to main schema .sql file.
        /// </summary>
        public static void InitialiseDb(string dbPath, string schemaPath)
        {
            // Create new database file if not already existing
            using (var conn = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = dbPath }.ToString()))
            {
                conn.Open();

                // Read main schema file to reset database
                using (var sqlFileStream = new FileInfo(schemaPath).OpenRead())
                {
                    using (var reader = new StreamReader(sqlFileStream, Encoding.UTF8))
                    {
                        var sqlCommand = reader.ReadToEnd();

                        // Create a new SQL command with contents of our schema file
                        using (var command = conn.CreateCommand())
                        {
                            command.CommandText = sqlCommand;

                            // Execute command and reset database
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }
    }
}
