﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;

namespace scada_server.Controllers
{
    public class DataController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public DataController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        
        /// <summary>
        /// Sends SQLite database file to client.
        /// </summary>
        /// <returns>SQLite .db file</returns>
        [HttpGet]
        public ActionResult Index()
        {
            var contentType = "application/x-sqlite3";
            HttpContext.Response.ContentType = contentType;
            var dbPath = _hostingEnvironment.ContentRootPath + Constants.DatabaseFileName;
            return new FileContentResult(System.IO.File.ReadAllBytes(dbPath), contentType);
        }

        /// <summary>
        /// GET /Data/GetDatabaseVersion: Gets current version identifier of server database.
        /// </summary>
        /// <returns>Version identifier.</returns>
        [HttpGet]
        public string GetDatabaseVersion()
        {
            string version = string.Empty;

            // Get current correct location of db file
            var dbPath = _hostingEnvironment.ContentRootPath + Constants.DatabaseFileName;

            // IMPORTANT: must always use 'using' syntax for connections, commands and readers
            using (var conn = new SqliteConnection(new SqliteConnectionStringBuilder { DataSource = dbPath }.ToString()))
            {
                conn.Open();

                // Get server database version
                using (var command = conn.CreateCommand())
                {
                    command.CommandText = "SELECT Version FROM Database_Version";

                    // Read database
                    using (var data = command.ExecuteReader())
                    {
                        // Version will be only column in only row
                        if (data.Read())
                        {
                            version = data[0].ToString();
                        }
                    }
                }
            }

            // Return version as string to client
            return version;

        }
    }
}
