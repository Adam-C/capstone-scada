﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace scada_server
{
    public static class Constants
    {
        // Platform-agnostic directory separator
        private static readonly string _sep = Path.DirectorySeparatorChar.ToString();

        public static readonly string DatabaseFileName = $"{_sep}AppData{_sep}VulnDefinitions.db";
            
        public static readonly string SchemaSqlFileName = $"{_sep}AppData{_sep}Schema.sql";

        public static readonly string LogDirectory = $".{_sep}LogFiles";

        public static readonly string LogFilePath = $".{_sep}LogFiles{_sep}ActivityLog.log";

        // How often to update database (in hours)
        public static int DatabaseUpdateWaitTimeHours = 6;
    }
}
