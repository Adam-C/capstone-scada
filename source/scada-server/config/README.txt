The files in this folder are to assist in server deployment.

"nginx-default" is a confguration file for Nginx to act as a reverse proxy for ASP.NET Core.

"scada-server.conf" is a configuration file for Supervisor (http://supervisord.org/) to manage the application process.

"deploy-scada-server.sh" is a Bash script to handle pulling the latest source (provided the Git repo has already been cloned), building it and starting it via Supervisor.