#!/bin/bash

# Get the latest source
cd ~/capstone-scada
git pull

# Stop process
sudo service supervisor stop

# Publish app and place files in correct location
cd ~/capstone-scada/source/scada-server/src/scada-server
dotnet restore
dotnet publish
sudo rm -rf /var/aspnetcore/publish
sudo cp -R ~/capstone-scada/source/scada-server/src/scada-server/bin/Debug/netcoreapp1.0/publish /var/aspnetcore/

# Fix permission errors (for db file)
sudo chown www-data -R /var/aspnetcore/publish

# Restart process and process manager
sudo service supervisor start