﻿using System;
using System.IO;
using System.Web.Http;
using System.Web.Http.SelfHost;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using scada_app;

namespace scada_xp
{
    class Program
    {
        static void Main()
        {
            FileStream file;

            // Initialise log
            if (!File.Exists(Constants.ActivityLogFilePath))
            {
                Directory.CreateDirectory(Constants.LogDirectory);
                file = File.Create(Constants.ActivityLogFilePath);
                file.Dispose();
            }

            // Initialise temp scan activity log
            if (!File.Exists(Constants.ProgressUpdateFilePath))
            {
                Directory.CreateDirectory(Constants.ProgressUpdateDirectory);
                file = File.Create(Constants.ProgressUpdateFilePath);
                file.Dispose();
            }

            var config = new HttpSelfHostConfiguration("http://localhost:5000");
            
            config.Routes.MapHttpRoute(
                "App default route", "{controller}/{action}");

            //// Increase max message size because of large JSON
            config.MaxReceivedMessageSize = 1048576;
            
            // Suppress warnings from Razor Engine
            var templateConfig = new TemplateServiceConfiguration();
            templateConfig.CachingProvider = new DefaultCachingProvider(t => {});
            var templateService = RazorEngineService.Create(templateConfig);
            RazorEngine.Engine.Razor = templateService;

            using (HttpSelfHostServer server = new HttpSelfHostServer(config))
            {
                server.OpenAsync().Wait();
                Console.WriteLine("*** SCADA Vulnerability Scanner ***");
                Console.WriteLine("Running at localhost:5000");
                Console.WriteLine("Press enter to quit...");
                Console.ReadLine();
            }
        }

        public static class State
        {
            private static bool AbortScan = false;

            public static bool ContinueScan()
            {
                if (AbortScan)
                {
                    SetAbortScan(false);
                    return false;
                }
                else
                {
                    return true;
                }
            }

            public static void SetAbortScan(bool status)
            {
                AbortScan = status;
            }
        }
    }
}
