﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using scada_app;
using scada_app.Logic;

namespace scada_xp.Controllers
{
    public class LogicController : ApiController
    {
        #region View Utility

        private HttpResponseMessage ViewResponse(string view, dynamic model)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(new ViewEngine.HtmlActionResult(view, model).Render());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        #endregion

        #region Database

        [HttpPost]
        public HttpResponseMessage SetupDb()
        {
            var message = LocalDb.SetupDb();

            return ViewResponse("DatabaseUpdateResults", new {Message = message});
        }

        #endregion

        #region Scan Single Address

        [HttpPost, HttpGet]
        public HttpResponseMessage ScanSingleAddress(string ipAddress)
        {
            Program.State.SetAbortScan(false);

            // Reset temp scan activity file
            ProgressUpdate.ClearFile();

            var actionData = new ActionData();
            actionData.ScanResult = new ScanResult();

            Log.WriteMessage("Beginning single address scan at " + ipAddress, Constants.ActivityLogFilePath, true);

            actionData.ScanRequest.IpAddress = ipAddress;

            Scan.Perform(actionData);

            return ViewResponse("DeviceResults", actionData);
        }

        [HttpPost]
        public HttpResponseMessage GetUpdateScanSingleAddress()
        {
            var update = new ScanSingleAddressUpdate();

            update.Messages = ProgressUpdate.GetMessages();

            return ViewResponse("ScanSingleAddressUpdate", update);
        }

        public class ScanSingleAddressUpdate
        {
            public List<string> Messages;
        }

        #endregion

        #region Scan Entire Network

        [HttpPost, HttpGet]
        public HttpResponseMessage ScanEntireNetwork(string localIp)
        {
            Program.State.SetAbortScan(false);

            // Reset temp scan activity file
            ProgressUpdate.ClearFile();

            var actionData = new ActionData();
            actionData.ScanResult = new ScanResult();

            Log.WriteMessage("Starting entire network scan", Constants.ActivityLogFilePath, true);
            ProgressUpdate.WriteActivityMessage("Starting entire network scan");

            var possibleHosts = Scan.GetPossibleHosts(localIp);

            foreach (var host in possibleHosts)
            {
                if (!Program.State.ContinueScan()) { return null; }

                // Set new scanrequest IP address
                actionData.ScanRequest.IpAddress = host;

                // Perform a scan on that address
                // Shouldn't add device and should immediately stop if tcp connection can't be made
                Scan.Perform(actionData);
            }

            return ViewResponse("DeviceResults", actionData);
        }

        #endregion

        #region Abort Scan

        [HttpPost]
        public void AbortScan()
        {
            Program.State.SetAbortScan(true);
        }

        #endregion

        #region Display Results

        [HttpPost]
        public HttpResponseMessage GetDeviceVulnerabilities(JToken jsonString)
        {
            // Manually perform model binding
            Device device = jsonString.ToObject<Device>();

            return ViewResponse("VulnerabilityResults", device);
        }

        [HttpPost]
        public HttpResponseMessage GetVulnerabilityMitigations(JToken jsonString)
        {
            // Manually perform model binding
            Vulnerability vulnerability = jsonString.ToObject<Vulnerability>();

            return ViewResponse("MitigationResults", vulnerability);
        }

        #endregion

        #region Progress Updates

        [HttpPost]
        public HttpResponseMessage GetProgressUpdate()
        {
            return ViewResponse("GetProgressUpdate", null);
        }

        #endregion

        #region Network Option

        [HttpPost]
        public string GetAvailableNetworks()
        {
            var availableNetworks = new List<string>();

            foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                // check interface is up
                if (networkInterface.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (UnicastIPAddressInformation addressInformation in networkInterface.GetIPProperties().UnicastAddresses)
                    {
                        // check address is IPv4
                        if (addressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            // check address isn't loopback
                            if (!IPAddress.IsLoopback(addressInformation.Address))
                            {
                                availableNetworks.Add(addressInformation.Address.ToString());
                            }
                        }
                    }
                }
            }

            // Include only unique entries
            availableNetworks = availableNetworks.Distinct().ToList();

            // Return JSON to the client so we can populate a dropdown list on UI

            return JsonConvert.SerializeObject(availableNetworks);
        }

        #endregion
    }
}
