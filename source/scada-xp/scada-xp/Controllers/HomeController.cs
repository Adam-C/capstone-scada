﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using scada_app;

namespace scada_xp
{
    public class HomeController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Index()
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(new ViewEngine.HtmlActionResult("Index", null).Render());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        // ------------------------- Static file controllers below

        [HttpGet]
        public HttpResponseMessage Scripts(string file)
        {
            var path = Path.Combine(Constants.WwwRootPath, "scripts\\" + file);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("text/javascript");

            return result;
        }

        [HttpGet]
        public HttpResponseMessage Styles(string file)
        {
            var path = Path.Combine(Constants.WwwRootPath, "styles\\" + file);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("text/css");

            return result;
        }

        [HttpGet]
        public HttpResponseMessage Img(string file)
        {
            var path = Path.Combine(Constants.WwwRootPath, "img\\" + file);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(path, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("image/xyz");

            return result;
        }
    }
}
