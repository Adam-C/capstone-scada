﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Data.SQLite;
using Dapper;

namespace scada_app.Logic
{
    public static class LocalDb
    {

        #region Setup

        /// <summary>
        /// Gets a local SQLite connection and creates new database file if necessary.
        /// </summary>
        /// <returns>Connection to SQLite database.</returns>
        private static SQLiteConnection GetConnection()
        {
            return new SQLiteConnection("Data Source=" + Constants.DatabaseFilePath + ";Version=3;");
        }

        /// <summary>
        /// Checks if database needs updating and retrieves new database if necessary.
        /// </summary>
        /// <returns>Message describing outcome.</returns>
        public static string SetupDb()
        {
            var localVersion = string.Empty;
            var serverVersion = string.Empty;

            using (var conn = GetConnection())
            {
                conn.Open();
                
                // Get local db version
                try
                {
                    using (var command = conn.CreateCommand())
                    {
                        command.CommandText = "SELECT Version_ID FROM Database_Version";

                        // Read database
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Version will be first (and only) column
                                localVersion = reader[0].ToString();
                            }
                        }
                    }
                }
                // TODO: better error handling for when Database_Version table does not exist (due to db not being fully setup yet
                catch { }
            }            

            // Get server db version
            try
            {
                serverVersion = RetrieveServerDatabaseVersion();

                // If local db is out of date
                if (localVersion != serverVersion)
                {
                    RetrieveExternalDatabase();

                    return "Local database updated from server";
                }
                else
                {
                    return "Local database already up-to-date";
                }
            }
            catch
            {
                return "Error: Unable to update local database at this time";
            }
            
        }

        private static string RetrieveServerDatabaseVersion()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(Constants.ServerDatabaseAddress + "/Data/GetDatabaseVersion");
            // Will result in exception if timeout is reached
            client.Timeout = TimeSpan.FromMilliseconds(Constants.TimeoutDbConnection);

            return client.GetAsync(string.Empty).Result.Content.ReadAsStringAsync().Result;
        }

        private static void RetrieveExternalDatabase()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(Constants.ServerDatabaseAddress + "/Data");

            // TODO: add error handling for database server problems
            var responseStream = client.GetStreamAsync(string.Empty).Result;

            // Create or overwrite file and write response to it
            var fileStream = File.Create(Constants.DatabaseFilePath);
            
            if (responseStream != null)
            {
                fileStream.Position = 0;

                // Copy received db file to local file
                responseStream.CopyTo(fileStream);
                responseStream.Dispose();
            }

            fileStream.Flush();
            fileStream.Dispose();
        }

        #endregion

        #region Lookups

        public static void LookupDatabaseVulnerabilities(ActionData actionData)
        {
            LookupWithProtocol(actionData);

            // TODO: add more search types (LookupWithModel, LookupWithManufacturer or more complex ones, etc.)
        }

        private static void LookupWithProtocol(ActionData actionData)
        {
            var device = actionData.GetCurrentDevice();

            using (var conn = GetConnection())
            {
                conn.Open();

                // Use Dapper ORM to query vulnerabilities and map to our Vulnerability object
                var vulns =
                    conn.Query<Vulnerability>(
                        $"SELECT ID, Name, Description, CVSS FROM Vulnerabilities WHERE Protocol = '{device.Protocol}'");
                    
                // Add all vulnerabilities to the device
                foreach (var vuln in vulns)
                {
                    // Need to manually add all reference links first
                    using (var command = conn.CreateCommand())
                    {
                        command.CommandText = $"SELECT Link FROM Reference_Links WHERE ID = {vuln.Id};";

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var link = reader.GetString(0);

                                vuln.ReferenceLinks.Add(link);
                            }
                        }
                    }
                    device.Vulnerabilities.Add(vuln);
                }
            }
        }

        public static void LookupDatabaseMitigations(ActionData actionData)
        {
            var device = actionData.GetCurrentDevice();

            using (var conn = GetConnection())
            {
                conn.Open();

                foreach (var vuln in device.Vulnerabilities)
                {
                    // If vulnerability does not have valid ID we cannot find mitigations for it
                    if (vuln.Id == -1) continue;

                    var mitigations =
                        conn.Query<Mitigation>(
                            $"SELECT ID, Description, Reference_Link FROM Mitigations WHERE Vulnerability_ID = {vuln.Id}");

                    foreach (var mitigation in mitigations)
                    {
                        vuln.Mitigations.Add(mitigation);
                    }
                }
            }
        }

        #endregion

    }
}
