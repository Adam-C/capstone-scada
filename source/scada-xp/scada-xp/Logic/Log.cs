﻿using System;
using System.IO;

namespace scada_app.Logic
{
    public static class Log
    {
        private static StreamWriter GetStreamWriter(string path)
        {
            return File.AppendText(path);
        }

        public static void WriteMessage(string message, string path, bool withCurrentTime = false)
        {
            using (var writer = GetStreamWriter(path))
            {
                if (withCurrentTime)
                {
                    // Add date time
                    message = DateTime.Now + " -- " + message;
                }

                writer.WriteLine(message);
            }
        }
    }
}
