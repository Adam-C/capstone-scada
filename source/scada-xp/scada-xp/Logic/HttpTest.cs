﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace scada_app.Logic
{
    public static class HttpTest
    {
        private static readonly List<string> Ports = new List<string>
        {
            "80", "3580", "8080",
        };

        // Check for connection on all possible ports
        public static void CheckService(ActionData actionData)
        {
            ProgressUpdate.WriteActivityMessage("Checking for active web service");
            foreach (var port in Ports)
            {
                try
                {
                    string uri = $"http://{actionData.ScanRequest.IpAddress}:{port}";

                    var requestClient = new HttpClient();
                    requestClient.BaseAddress = new Uri(uri);

                    var responseString = requestClient.GetAsync(string.Empty).Result.Content.ReadAsStringAsync().Result;

                    var device = actionData.GetCurrentDevice();

                    // Only add a single HTTP vulnerability (for now)
                    if (device != null && !string.IsNullOrEmpty(responseString) && device.Vulnerabilities.All(v => v.Type != Constants.Vulnerability.Http.Name))
                    {
                        device.Vulnerabilities.Add(new Vulnerability
                        {
                            Type = Constants.Vulnerability.Http.Name,
                            ManuallyDetected = true,
                            Description = "Active web server receiving connections",
                        });

                        // EIP only: Perform web scraping
                        if (device.Protocol == Constants.Protocols.EthernetIP.Name)
                        {
                            ProtocolTests.EthernetIPTest.ScrapeWebServer(actionData);
                        }

                    }

                    // Suppress errors if test unsuccessful
                } catch { }
                
            }
        }
    }
}
