﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace scada_app.Logic
{
    public static class FtpTest
    {
        public static void CheckLogin(string uri, ActionData actionData)
        {
                
            ProgressUpdate.WriteActivityMessage("Attempting unauthorized FTP login");
            // TODO: do this in a better way than just using a try/catch
            try
            {
                var request = (FtpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                var response = (FtpWebResponse)request.GetResponse();

                // TODO: find out proper way to determine if request was successful
                if (response.ContentLength > 0)
                {
                   // Get current device and add FTP vulnerability
                   actionData.GetCurrentDevice().Vulnerabilities.Add(new Vulnerability
                   {
                       Type = Constants.Vulnerability.Ftp.Name,
                       ManuallyDetected = true,
                       Description = Constants.VulnerabilityDescriptions.Generic.AnonFtp,
                       Severity = "Medium",
                       Mitigations = new List<Mitigation>
                       {
                           new Mitigation
                           {
                               Description = Constants.MitigationDescriptions.Generic.AnonFTP,
                           }
                       }
                   });
                }
                
                response.Close();

            } catch { }
            

        }
    }

    
}
