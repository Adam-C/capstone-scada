﻿
using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using SnmpSharpNet;

namespace scada_app.Logic {

    public static class SnmpTest {

        public static void RunAll(ActionData actionData)
        {
            // Initialise Snmp log
            if (!File.Exists(Constants.SnmpLogFilePath))
            {
                if (!Directory.Exists(Constants.LogDirectory))
                {
                    Directory.CreateDirectory(Constants.LogDirectory);
                }
                FileStream file = File.Create(Constants.SnmpLogFilePath);
                file.Dispose();
            }

            if (SimpleSnmpTest(actionData)) {
                SnmpWalk(actionData);
            }
        }

        public static bool SimpleSnmpTest(ActionData actionData) {

            bool isActive = false;

            // Default SNMP Community Name
            OctetString communityName = new OctetString("public");

            // Set up new agent parameters
            AgentParameters parameters = new AgentParameters(communityName);

            // Specify SNMP version
            parameters.Version = SnmpVersion.Ver2;

            // Try to parse ip address
            IPAddress host = IPAddress.Parse(actionData.ScanRequest.IpAddress);

            // Construct the target
            UdpTarget target = new UdpTarget(host, 161, 2000, 1);

            // Specify type of Get request
            Pdu pduGet = new Pdu(PduType.Get);

            pduGet.VbList.Add("1.3.6.1.2.1.1.1.0"); // System Description

            // Perfom SNMP request
            try {
                ProgressUpdate.WriteActivityMessage("Making SNMP-Get request");
                SnmpV2Packet getSystemDescription = (SnmpV2Packet)target.Request(pduGet, parameters);

                //Check if agent didn't reply or response couldn't be parsed
                if (getSystemDescription != null) {
                    // Check for error in SNMP response
                    if (getSystemDescription.Pdu.ErrorStatus != 0) {
                        ProgressUpdate.WriteActivityMessage("SNMP Agent didn't reply or response couldn't be parsed");
                    } else {
                        actionData.GetCurrentDevice().Vulnerabilities.Add(
                            new Vulnerability {
                                Type = Constants.Vulnerability.Snmp.Name,
                                Severity = "Medium",
                                ManuallyDetected = true,
                                Description = Constants.VulnerabilityDescriptions.Generic.PublicSNMP,
                                Mitigations = new List<Mitigation>
                                {
                                    new Mitigation
                                    {
                                        Description = Constants.MitigationDescriptions.Generic.PublicSNMP,
                                    }
                                }
                            });
                        isActive = true;
                    }
                }
            } catch {
                //ProgressUpdate.WriteActivityMessage("There is no response from SNMP agent using community name \"public\".");
            }

            target.Close();
            return isActive;
        }

        public static void SnmpWalk(ActionData actionData) {

            string endOfMibView = "SNMP End-of-MIB-View";

            // Default SNMP Community Name
            OctetString communityName = new OctetString("public");

            // Agent parameters
            AgentParameters parameters = new AgentParameters(communityName);

            // Specify SNMP version
            parameters.Version = SnmpVersion.Ver2;

            // Try to parse ip address
            IPAddress host = IPAddress.Parse(actionData.ScanRequest.IpAddress);

            // Construct the target
            UdpTarget target = new UdpTarget(host, 161, 2000, 1);

            Oid rootOid = new Oid("1.3.6.1.2.1"); //root Oid of MIB tree
            Oid lastOid = (Oid)rootOid.Clone(); //last Oid of MIB tree

            // Specify type of Get request
            Pdu pdu = new Pdu(PduType.GetNext);

            // Start log entry
            Log.WriteMessage("SNMP values for device at " + actionData.ScanRequest.IpAddress, Constants.SnmpLogFilePath, true);
            ProgressUpdate.WriteActivityMessage("Perfoming SNMPWalk Test");

            while (lastOid != null) {

                // Clear the List of Oids
                pdu.VbList.Clear();

                // Initialize request PDU with the last retrieved Oid
                pdu.VbList.Add(lastOid);

                // Perfom SNMP request
                try {
                    SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, parameters);

                    //If result equals null, it means agent didn't reply or it's reply cannot be parsed
                    if (result != null) {
                        // Check for error in SNMP response
                        if (result.Pdu.ErrorStatus != 0) {
                            lastOid = null;
                            break;
                        } else {

                            // Walk through returned values
                            foreach (Vb v in result.Pdu.VbList) {

                                // check if end of MIB tree is reached
                                if (v.Value.ToString() == endOfMibView) {
                                    lastOid = null;

                                // Check that retrieved Oid is "child" of the root Oid
                                } else if (rootOid.IsRootOf(v.Oid)) {
                                    // Add results to log
                                    Log.WriteMessage("Oid: " + v.Oid.ToString(), Constants.SnmpLogFilePath);
                                    Log.WriteMessage("Value: " + v.Value.ToString(), Constants.SnmpLogFilePath);
                                    lastOid = v.Oid;
                                } else {
                                    lastOid = null;
                                }
                            }
                        }
                    }
                } catch {
                    //ProgressUpdate.WriteActivityMessage("There is no response from SNMP agent using community name \"public\".");
                }
            }

            target.Close();
        }
    }
}
