﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace scada_app.Logic.ProtocolTests
{
    public static class ModbusTest
    {
        public static void RunAll(ActionData data)
        {
            // Initialise modbus log
            if (!File.Exists(Constants.ModbusLogFilePath))
            {
                if (!Directory.Exists(Constants.LogDirectory))
                {
                    Directory.CreateDirectory(Constants.LogDirectory);
                }
                FileStream file = File.Create(Constants.ModbusLogFilePath);
                file.Dispose();
            }


            ProgressUpdate.WriteActivityMessage("Attempting to read coils");
            ReadCoils(data);

            ProgressUpdate.WriteActivityMessage("Attempting to read discrete inputs");
            ReadDiscreteInputs(data);

            ProgressUpdate.WriteActivityMessage("Attempting to read holding registers");
            ReadHoldingRegisters(data);

            ProgressUpdate.WriteActivityMessage("Attempting to read input registers");
            ReadInputRegisters(data);

            ProgressUpdate.WriteActivityMessage("Attempting to read device ID");
            ReadDeviceId(data);
        }

        // Must be called immediately after adding the function vulnerability
        private static void AddModbusSpecLink(ActionData actionData) {
            actionData.GetCurrentDevice().Vulnerabilities.Last().ReferenceLinks.Add(Constants.Links.ModbusSpecSheet);
        }


        #region Function Codes
        
        // Function 01 - CONFIRMED WORKING
        private static void ReadCoils(ActionData actionData)
        {
            // set function flags
            byte function = 0x01;                   // Read coils modbus function
            byte[] startAddress = { 0x00, 0x0D };   // Address of the first coil to be read
            byte[] quantity = { 0x00, 0x0D };       // Amount of coils to read

            byte[] functionFlags = {
                function,               
                startAddress[0], startAddress[1],
                quantity[0], quantity[1]         
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "Medium",
                        Description = "It is possible for an attacker to read the status" + 
                        "of the coils in a remote slave device using function code 1. " + 
                        "The status of the coil is indicated as 1 = ON and 0 = OFF.  " + 
                        "Potentially the attacker may use this information to profile the system.",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });
                    
                // Add a link to Modbus specification PDF
                AddModbusSpecLink(actionData);

                var dataLength = GetFunctionResponseLength(response, 2);

                // Write response to log
                Log.WriteMessage("Read coils of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Byte Count: " + response[8], Constants.ModbusLogFilePath);
                Log.WriteMessage("Coil Status: " + GetDataString(response, 9, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 02 - CONFIRMED WORKING
        private static void ReadDiscreteInputs(ActionData actionData)
        {
            // set function flags
            byte function = 0x02;                   // Read discrete inputs modbus function
            byte[] startAddress = { 0x00, 0x0D };   // Address of the first discrete input to be read
            byte[] quantity = { 0x00, 0x0D };       // Amount of inputs to read

            byte[] functionFlags = {
                function,
                startAddress[0], startAddress[1],
                quantity[0], quantity[1]
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "Medium",
                        Description = "It is possible for an attacker to read " + 
                        "the discrete inputs from a Modbus slave device using function code 2. " + 
                        "The status is indicated as 1 = ON and 0 = OFF.  " + 
                        "Potentially, the ability to read the discrete inputs " + 
                        "may be used for an attacker to profile the system.",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                AddModbusSpecLink(actionData);

                var dataLength = GetFunctionResponseLength(response, 2);

                // Write response to log
                Log.WriteMessage("Read discrete inputs of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Byte Count: " + response[8], Constants.ModbusLogFilePath);
                Log.WriteMessage("Input Status: " + GetDataString(response, 9, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 03 - CONFIRMED WORKING
        private static void ReadHoldingRegisters(ActionData actionData)
        {
            // set function flags
            byte function = 0x03;                   // Read holding registers modbus function
            byte[] startAddress = { 0x00, 0x0D };   // Address of the first holding register to be read
            byte[] quantity = { 0x00, 0x0D };       // Amount of registers to read

            byte[] functionFlags = {
                function,
                startAddress[0], startAddress[1],
                quantity[0], quantity[1]
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "Medium",
                        Description = "It is possible for an attacker to read the content " + 
                        "of the holding registers from a remote Modbus slave device " + 
                        "using function code 3. The ability to read the holding registers " + 
                        "may be used for an attacker to profile the system.",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                AddModbusSpecLink(actionData);
                
                var dataLength = GetFunctionResponseLength(response, 2);

                // Write response to log
                Log.WriteMessage("Read holding registers of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Byte Count: " + response[8], Constants.ModbusLogFilePath);
                Log.WriteMessage("Register Value: " + GetDataString(response, 9, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 04 - CONFIRMED WORKING
        private static void ReadInputRegisters(ActionData actionData)
        {
            // set function flags
            byte function = 0x04;                   // Read input registers modbus function
            byte[] startAddress = { 0x00, 0x0D };   // Address of the first input register to be read
            byte[] quantity = { 0x00, 0x0D };       // Amount of registers to read

            byte[] functionFlags = {
                function,
                startAddress[0], startAddress[1],
                quantity[0], quantity[1]
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "Medium",
                        Description = "It is possible for an attacker to read the input " + 
                        "registers from a remote Modbus slave device using function code 4. " + 
                        "The ability to read the input registers may be used for an attacker to profile the system.",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                AddModbusSpecLink(actionData);

                var dataLength = GetFunctionResponseLength(response, 2);

                // Write response to log
                Log.WriteMessage("Read input registers of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Byte Count: " + response[8], Constants.ModbusLogFilePath);
                Log.WriteMessage("Input Registers: " + GetDataString(response, 9, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 05 - CONFIRMED WORKING
        // Needs vulnerability information added
        private static void WriteSingleCoil(ActionData actionData)
        {
            // set function flags
            byte function = 0x05;                   // Write single coil modbus function
            byte[] outputAddress = { 0x00, 0x0D };  // Address of the coil to be write
            byte[] value = { 0xFF, 0x00 };          // Value to be written to coil. 0x0000 or 0xFF00

            byte[] functionFlags = {
                function,
                outputAddress[0], outputAddress[1],
                value[0], value[1]
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "",
                        Description = "",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                AddModbusSpecLink(actionData);

                // Write response to log
                Log.WriteMessage("Write single coil of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Output Address: " + response[8] + " " + response[9], Constants.ModbusLogFilePath);
                Log.WriteMessage("Output Value: " + response[10], Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 06 - UNCONFIRMED
        // Needs vulnerability information added
        private static void WriteSingleRegister(ActionData actionData)
        {
            // set function flags
            byte function = 0x06;                   // Write single register modbus function
            byte[] outputAddress = { 0x00, 0x0D };  // Address of the first register to be write
            byte[] value = { 0xFF, 0x00 };          // Value to be written to register.

            byte[] functionFlags = {
                function,
                outputAddress[0], outputAddress[1],
                value[0], value[1]
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "",
                        Description = "",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                // Write response to log
                Log.WriteMessage("Write register of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Register Address: " + response[8] + " " + response[9], Constants.ModbusLogFilePath);
                Log.WriteMessage("Register Value: " + response[10] + " " + response[11], Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 20 - UNCONFIRMED
        // Needs vulnerability information added
        private static void ReadFileRecord(ActionData actionData)
        {
            // set function flags
            byte function = 0x14;                   // Read file record modbus function
            byte length = 0x07;                     // Amount of bytes to follow
            byte referenceType = 0x06;              // Must be 0x06 as per specification
            byte[] fileNumber = { 0x00, 0x00 };     // File to be read
            byte[] recordNumber = { 0x00, 0x00 };   // Record to be read
            byte[] recordLength = { 0x00, 0x00 };   // Length of the record to read

            byte[] functionFlags = {
                function,
                length,
                referenceType,
                fileNumber[0], fileNumber[1],
                recordNumber[0], recordNumber[1],
                recordLength[0], recordLength[1],
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "",
                        Description = "",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });
                
                var dataLength = GetFunctionResponseLength(response, 2);

                // Write response to log
                Log.WriteMessage("Read file record of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Data Length: " + response[8], Constants.ModbusLogFilePath);
                Log.WriteMessage("Data: " + GetDataString(response, 9, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 21 - UNCONFIRMED
        // Needs vulnerability information added
        private static void WriteFileRecord(ActionData actionData)
        {
            // set function flags
            byte function = 0x15;                   // Write file record modbus function
            byte length = 0x09;                     // Amount of bytes to follow
            byte referenceType = 0x06;              // Must be 0x06 as per specification
            byte[] fileNumber = { 0x00, 0x00 };     // File to be write
            byte[] recordNumber = { 0x00, 0x00 };   // Record to be write
            byte[] recordLength = { 0x00, 0x00 };   // Length of the record to write
            byte[] recordData = { 0x00, 0x00 };     // Data to write

            byte[] functionFlags = {
                function,
                length,
                referenceType,
                fileNumber[0], fileNumber[1],
                recordNumber[0], recordNumber[1],
                recordLength[0], recordLength[1],
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "",
                        Description = "",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                var dataLength = GetFunctionResponseLength(response, 2);

                // Write response to log
                Log.WriteMessage("Write file record of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Data Length: " + response[8], Constants.ModbusLogFilePath);
                Log.WriteMessage("Data: " + GetDataString(response, 9, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 24 - UNCONFIRMED
        // Needs vulnerability information added
        private static void ReadFIFOQueue(ActionData actionData)
        {
            // set function flags
            byte function = 0x18;                   // Read FIFO queue modbus function
            byte[] pointerAddress = { 0x00, 0x09 };             // Register in the queue to read

            byte[] functionFlags = {
                function,
                pointerAddress[0], pointerAddress[1]
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "",
                        Description = "",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                var dataLength = GetFunctionResponseLength(response, 5);

                // Write response to log
                Log.WriteMessage("Read FIFO queue of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Byte Count: " + response[8] + " " + response[9], Constants.ModbusLogFilePath);
                Log.WriteMessage("FIFO Count: " + response[10] + " " + response[11], Constants.ModbusLogFilePath);
                Log.WriteMessage("FIFO Value Register: " + GetDataString(response, 12, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        // Function 43 - CONFIRMED NOT RECOGNISED BY LAB DEVICE
        // Needs vulnerability information added
        private static void ReadDeviceId(ActionData actionData)
        {
            // set function flags
            byte function = 0x2B;           // function code
            byte MEIType = 0x0E;            // Modbus Encapsulated Interface Type
            byte ReadDeviceIdCode = 0x01;   // 0x01 - 0x04 are valid. See Modbus application Protocol Specification for more details
            byte ObjectId = 0x00;           // see Modbus application Protocol Specification for more details


            byte[] functionFlags = {
                function,
                MEIType,
                ReadDeviceIdCode,
                ObjectId
            };

            var request = BuildModbusPacket(functionFlags, 0);

            var response = SendAndReceiveData(request, Constants.Protocols.Modbus.Port, actionData.ScanRequest.IpAddress);

            if (!IsEmptyResponse(response) && !IsErrorResponse(response, function))
            {
                actionData.GetCurrentDevice().Vulnerabilities.Add(
                    new Vulnerability
                    {
                        Type = Constants.Protocols.Modbus.Name,
                        ManuallyDetected = true,
                        Severity = "",
                        Description = "",
                        Mitigations = new List<Mitigation>
                        {
                            new Mitigation
                            {
                                Description = Constants.MitigationDescriptions.Modbus.ModbusMitigation,
                            }
                        }
                    });

                var dataLength = GetFunctionResponseLength(response, 1);

                // Write response to log
                Log.WriteMessage("Read device information of device at " + actionData.ScanRequest.IpAddress, Constants.ModbusLogFilePath, true);
                LogTCPAndFunctionResponse(response);
                // Write function specific response fields
                Log.WriteMessage("Data: " + GetDataString(response, 8, dataLength), Constants.ModbusLogFilePath);
            }

            // Log error if applicable
            if (IsErrorResponse(response, function))
                LogErrorResponse(response);
        }

        #endregion

        #region Utilities

        // builds modbus packets
        private static byte[] BuildModbusPacket(byte[] functionFlags, uint transactionIdInt)
        {
            // set TCP/IP flags
            byte[] transactionId = ByteUtilities.UIntToBytes(transactionIdInt, 2);   // Transaction ID of the communication
            byte[] protocol = { 0x00, 0x00 };   // Modbus protocol (0x0000)
            // add one to include the slaveId byte      
            byte[] dataLength = ByteUtilities.UIntToBytes(Convert.ToUInt32(functionFlags.Length) + 1, 2);  // Amount of bytes to follow
            byte slaveId = 0x01;    // Slave ID of the destination device

            // build packet starting with TCP/IP flags
            List<byte> modbusPacket = new List<byte>(new byte[] {
                transactionId[0], transactionId[1],
                protocol[0], protocol[1],
                dataLength[0], dataLength[1],
                slaveId
            });

            // append function flags
            for (int i = 0; i < functionFlags.Length; i++)
            {
                modbusPacket.Add(functionFlags[i]);
            }

            return modbusPacket.ToArray();
        }

        /// <summary>
        /// Sends a Modbus request packet and returns the response
        /// </summary>
        /// <param name="request"> the request packet to be sent </param>
        /// <param name="port"> the destination port </param>
        /// <param name="ipAddress"> the destination ip address </param>
        /// <returns> the response from the destination device, or an empty byte array if no response is available </returns>
        private static byte[] SendAndReceiveData(byte[] request, int port, string ipAddress)
        {
            // Connect
            TcpClient client = new TcpClient();
            
            client.Connect(ipAddress, port);
            
            // Wait until connected (TODO: better approach with await)
            while (!client.Connected) { }

            // Send packet
            NetworkStream stream = client.GetStream();
            stream.Write(request, 0, request.Length);

            // Get response
            byte[] response = new byte[256];
            int bytes = 0;

            // Wait for response to become available or timeout
            var timer = System.Diagnostics.Stopwatch.StartNew();
            while (!stream.DataAvailable && timer.ElapsedMilliseconds < Constants.TimeoutProtocolConnection)
            {
                System.Threading.Thread.Sleep(Constants.TimeoutProtocolConnection);
            }

            // Read response if available
            if (stream.DataAvailable)
            {
                bytes = stream.Read(response, 0, response.Length);
            }

            // Close connections
            stream.Dispose();
            //client.Dispose();
            //stream.Close();
            //client.Close();

            return response;
        }

        /// <summary>
        /// Checks whether a response is an empty byte array
        /// </summary>
        /// <param name="response"></param>
        /// <returns> true if the array is empty, otherwise false </returns>
        private static bool IsEmptyResponse(byte[] response)
        {
            for (int i = 0; i < response.Length; i++)
            {
                if (response[i] != 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Checks whether the response returns an error code
        /// </summary>
        /// <param name="response"> the response from the device </param>
        /// <param name="function"> the function code of the request </param>
        /// <returns></returns>
        private static bool IsErrorResponse(byte[] response, byte function)
        {
            if (response[7] == function + 0x80)
                return true;
            else
                return false;
        }

        private static string GetDataString(byte[] response, int startIndex, uint dataLength)
        {
            var dataString = String.Empty;
            for (int i = startIndex; i < (startIndex + dataLength); i++)
            {
                dataString = dataString + response[i] + " ";
            }
            return dataString;
        }

        /// <summary>
        /// Logs the Modbus TCP/IP flags and function code a response
        /// </summary>
        /// <param name="response"> the response from the device </param>
        private static void LogTCPAndFunctionResponse (byte[] response)
        {
            Log.WriteMessage("Transaction identifier: " + response[0] + " " + response[1], Constants.ModbusLogFilePath);
            Log.WriteMessage("Protocol identifier: " + response[2] + " " + response[3], Constants.ModbusLogFilePath);
            Log.WriteMessage("Length: " + response[4] + " " + response[5], Constants.ModbusLogFilePath);
            Log.WriteMessage("Unit identifier: " + response[6], Constants.ModbusLogFilePath);
            Log.WriteMessage("Function Code: " + response[7], Constants.ModbusLogFilePath);
        }

        /// <summary>
        /// Logs an error response
        /// </summary>
        /// <param name="response"> the response from the device </param>
        private static void LogErrorResponse(byte[] response)
        {
            Log.WriteMessage("Error with function code " + response[0], Constants.ModbusLogFilePath, true);
            Log.WriteMessage("Exception code: " + response[1], Constants.ModbusLogFilePath);
        }

        private static uint GetFunctionResponseLength(byte[] response, int nonDataByteAmount)
        {
            var lengthBytes = new byte[2];
            // copy values 4 & 5 from response array into lengthBytes
            Array.Copy(response, 4, lengthBytes, 0, 2);
            // -1 for the unit identifier field in modbus TCP/IP response segment 
            var length = ByteUtilities.BytesToUInt(lengthBytes) - 1 - (uint)nonDataByteAmount;
            return length;
        }
        #endregion
    }
}
