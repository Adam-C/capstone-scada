﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scada_app
{
    /// <summary>
    /// Main data class. Passed throughout application (client and server).
    /// Objects here need property names exactly matching field names from the database.
    /// </summary>
    public class ActionData
    {
        public ScanRequest ScanRequest = new ScanRequest();
        public ScanResult ScanResult = new ScanResult();

        public Device GetCurrentDevice()
        {
            return this.ScanResult.Devices[this.ScanRequest.CurrentDeviceIndex];
        }
    }

    public class ScanRequest
    {
        public string IpAddress;

        // Keeps track of which device is being tested during scan
        public int CurrentDeviceIndex = -1;
    }

    public class ScanResult
    {
        public List<Device> Devices = new List<Device>();
    }

    public class Device
    {
        public string Description { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string Protocol { get; set; }

        public string IpAddress { get; set; }

        public string FirmwareVersion { get; set; }

        /// <summary>
        /// EIP only: Serial Number of the device.
        /// </summary>
        public uint SerialNumber { get; set; }

        /// <summary>
        /// EIP only: Revision number of the device.
        /// </summary>
        public string RevisionNumber { get; set; }

        /// <summary>
        /// EIP only: Product code of the device.
        /// </summary>
        public ushort ProductCode { get; set; }

        /// <summary>
        /// EIP only: Item type code of the device.
        /// </summary>
        public ushort ItemType { get; set; }

        public List<Vulnerability> Vulnerabilities = new List<Vulnerability>();
    }

    public class Vulnerability
    {
        public int Id = -1;

        public string Name = string.Empty;

        public string Type = string.Empty;

        public string Description = string.Empty;

        public string CVSS = string.Empty;

        public string Severity = string.Empty;

        // Indicates whether this was found by a specific vulnerability test.
        // (As opposed to a generic one from the database)
        public bool ManuallyDetected = false;

        public List<string> ReferenceLinks = new List<string>();

        public List<Mitigation> Mitigations = new List<Mitigation>();
    }

    public class Mitigation
    {
        public int Id = -1;

        public string Description = string.Empty;

        public string Reference_Link = string.Empty;
    }
}
