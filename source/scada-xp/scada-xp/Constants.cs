﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scada_app
{
    public static class Constants
    {
        // Platform-agnostic directory separator
        private static readonly string _sep = "\\";

        private static readonly string _currentLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public static readonly string WwwRootPath = _currentLocation + $"{_sep}wwwroot";

        public static readonly string ViewDirectory = _currentLocation + $"{_sep}Views";

        // TODO: configure with external host address
        public const string ServerDatabaseAddress = "http://40.126.248.194";

        #region File Paths

        public const string DatabaseFileName = "VulnDefinitions.db";

        public static readonly string DatabaseFilePath = _currentLocation + $"{_sep}" + DatabaseFileName;

        public static readonly string LogDirectory = _currentLocation + $"{_sep}LogFiles";

        public static readonly string ProgressUpdateDirectory = _currentLocation + $"{_sep}LogFiles{_sep}ProgressUpdate";

        public static readonly string ModbusLogFilePath = _currentLocation + $"{_sep}LogFiles{_sep}ModbusLog.log";

        public static readonly string SnmpLogFilePath = _currentLocation + $"{_sep}LogFiles{_sep}SnmpLog.log";

        public static readonly string ActivityLogFilePath = _currentLocation + $"{_sep}LogFiles{_sep}ActivityLog.log";

        public static readonly string ProgressUpdateFilePath = _currentLocation + $"{_sep}LogFiles{_sep}ProgressUpdate{_sep}Updates.log";

        #endregion

        /// <summary>
        /// Timeout length (in ms) to be used when attempting connections.
        /// </summary>
        public const int TimeoutProtocolConnection = 200;

        /// <summary>
        /// Timeout length (in ms) when attempting requests to database server;
        /// <summary>
        public const int TimeoutDbConnection = 15000;

        /// <summary>
        /// ODVA assigned VendorIDs for EthernetIP device vendors
        /// </summary>
        public static class EIPVendorID
        {
            /// <summary>
            /// holds the name and ODVA assigned VendorID for Allen Bradley/Rockwell Automation
            /// </summary>
            public static class AllenBradley
            {
                public const string Name = "Allen Bradley/Rockwell Automation";
                // identity information is sent from devices as type ushort
                // so it is stored this way for simplicity
                public const ushort ODVANum = 1;
            }

            // Add new EIPVendors here
        }

        public static class Protocols
        {
            public static class Modbus
            {
                public const string Name = "Modbus";
                public const int Port = 502;
            }

            /// <summary>
            /// This protocol is currently not implemented
            /// </summary>
            public static class Profinet
            {
                public const string Name = "PROFINET";
                public const int Port = 34962;  // For RT communication, may need 34963 or 34964 if testing is unsuccessful
            }

            public static class EthernetIP
            {
                public const string Name = "Ethernet/IP";
                public const int Port = 44818; // For explicit (non-timesensitive) communication
            }
        }

        public static class Vulnerability
        {
            public static class Ftp
            {
                public const string Name = "FTP";
            }

            public static class Http
            {
                public const string Name = "HTTP";
            }

            public static class Snmp
            {
                public const string Name = "SNMP";
            }
        }

        /// <summary>
        /// Used to get a list of the currently implemented protocols.
        /// Add entries for newly implemented protocols.
        /// </summary>
        /// <returns>Dictionary containing Key = Name, Value = Port</returns>
        public static Dictionary<string, int> GetProtocols()
        {
            var protocols = new Dictionary<string, int>()
            {
                { Protocols.Modbus.Name, Protocols.Modbus.Port },
                // Profinet protocol is currently untested
                // { Protocols.Profinet.Name, Protocols.Profinet.Port },
                { Protocols.EthernetIP.Name, Protocols.EthernetIP.Port }
            };
            return protocols;
        }

        public static Dictionary<ushort, string> GetEIPVendorIDs()
        {
            var vendorIDs = new Dictionary<ushort, string>()
            {
                // add new entries here
                // Key = ODVANum, Value = NAME
                { EIPVendorID.AllenBradley.ODVANum, EIPVendorID.AllenBradley.Name }
            };
            return vendorIDs;
        }

        #region Vulnerability Descriptions 

        public static class VulnerabilityDescriptions
        {
            public static class Generic
            {
                public const string AnonFtp = @"The current configuration of the remote FTP server allows any user 
                    to connect and pass the authentication process using an anonymous user account. This allows a 
                    remote user to retrieve directories and access to files that are made available for anonymous accounts. 
                    For additional information please refer to https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-1999-0497";

                public const string PublicSNMP = @"Simple Network Management Protocol(SNMP) is used by administrators
                    for remote managing and monitoring of a device or computer on the network
                    The PUBLIC community string is used to obtain the SNMP data from the remote
                    device on the read only basis. The current community name (public)
                    can be easily guessed and potentially can be used by intruder to obtain
                    more information about the remote host, which may include network settings
                    interfaces and processes running on the remote system.";
            }
        }

        #endregion

        #region Mitigation Descriptions

        public static class MitigationDescriptions
        {
            public static class Modbus
            {
                public const string ModbusMitigation = @"Limit access to the Modbus TCP port 502 to authorised clients only.";
            }

            public static class EthernetIp
            {
                // Default mitigations can go here
            }

            public static class Generic
            {
                public const string PublicSNMP = @"Disable SNMP service if you don’t use it. 
                    Change community name string. Restrict access to port 161 to authorised users only. 
                    Make sure that public community name mode does not expose sensitive information.";

                public const string AnonFTP = @"Disable anonymous FTP authentication or make sure 
                    that FTP server is configured complying with your company’s security policies.";
            }
        }

        #endregion

        #region User Links

        public static class Links {

            public const string ModbusSpecSheet = "http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b.pdf";
            
        }

        #endregion

    }
}
