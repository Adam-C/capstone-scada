﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Web.Razor;
using scada_app;

namespace scada_xp
{
    public class ViewEngine
    {
        public class HtmlActionResult
        {
            private readonly string _view;
            private readonly dynamic _model;

            public HtmlActionResult(string viewName, dynamic model)
            {
                _view = LoadView(viewName);
                _model = model;
            }

            public string Render()
            {
                return RazorEngine.Razor.Parse(_view, _model);
            }

            private static string LoadView(string name)
            {
                var view = File.ReadAllText(Path.Combine(Constants.ViewDirectory, name + ".cshtml"));
                return view;
            }
        }
    }
}
