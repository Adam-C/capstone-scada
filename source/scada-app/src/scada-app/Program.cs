﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace scada_app
{
    public class Program
    {
        public static class State
        {
            private static bool AbortScan = false;

            public static bool ContinueScan()
            {
                if (AbortScan)
                {
                    SetAbortScan(false);
                    return false;
                }
                else
                {
                    return true;
                }
            }

            public static void SetAbortScan(bool status)
            {
                AbortScan = status;
            }
        }

        public static void Main()
        {
            FileStream file;

            // Initialise log
            if (!File.Exists(Constants.ActivityLogFilePath))
            {
                Directory.CreateDirectory(Constants.LogDirectory);
                file = File.Create(Constants.ActivityLogFilePath);
                file.Dispose();
            }

            // Initialise temp scan activity log
            if (!File.Exists(Constants.ProgressUpdateFilePath))
            {
                Directory.CreateDirectory(Constants.ProgressUpdateDirectory);
                file = File.Create(Constants.ProgressUpdateFilePath);
                file.Dispose();
            }

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())

                // Use the content root below only when publishing build
                //.UseContentRoot(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))

                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
