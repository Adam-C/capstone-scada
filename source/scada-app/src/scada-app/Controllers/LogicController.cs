﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using scada_app.Logic;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using Newtonsoft.Json;

namespace scada_app.Controllers
{
    // Used for all app actions
    public class LogicController : Controller
    {
        #region Database

        [HttpPost]
        public ViewResult SetupDb()
        {
            ViewData["Message"] = LocalDb.SetupDb();
            
            return View("DatabaseUpdateResults");
        }

        #endregion

        #region Scan Single Address

        [HttpPost]
        public ViewResult ScanSingleAddress(string ipAddress)
        {
            Program.State.SetAbortScan(false);

            // Reset temp scan activity file
            ProgressUpdate.ClearFile();

            var actionData = new ActionData();   
            actionData.ScanResult = new ScanResult();

            Log.WriteMessage("Beginning single address scan at " + ipAddress, Constants.ActivityLogFilePath, true);

            actionData.ScanRequest.IpAddress = ipAddress;

            Scan.Perform(actionData);

            return View("DeviceResults", actionData);
        }

        [HttpPost]
        public ViewResult GetUpdateScanSingleAddress()
        {
            var update = new ScanSingleAddressUpdate();

            update.Messages = ProgressUpdate.GetMessages();

            return View(update);
        }

        public class ScanSingleAddressUpdate
        {
            public List<string> Messages;
        }

        #endregion

        #region Scan Entire Network

        [HttpPost]
        public ViewResult ScanEntireNetwork(string localIp)
        {
            Program.State.SetAbortScan(false);

            // Reset temp scan activity file
            ProgressUpdate.ClearFile();

            var actionData = new ActionData();
            actionData.ScanResult = new ScanResult();

            Log.WriteMessage("Starting entire network scan", Constants.ActivityLogFilePath, true);
            ProgressUpdate.WriteActivityMessage("Starting entire network scan");

            var possibleHosts = Scan.GetPossibleHosts(localIp);

            foreach (var host in possibleHosts)
            {
                if (!Program.State.ContinueScan()) { return null; }

                // Set new scanrequest IP address
                actionData.ScanRequest.IpAddress = host;

                // Perform a scan on that address
                // Shouldn't add device and should immediately stop if tcp connection can't be made
                Scan.Perform(actionData);
            }
            
            return View("DeviceResults", actionData);
        }

        #endregion

        #region Abort Scan

        [HttpPost]
        public void AbortScan()
        {
            Program.State.SetAbortScan(true);
        }

        #endregion

        #region Display Results

        [HttpPost]
        public ViewResult GetDeviceVulnerabilities(string jsonString)
        {
            // Manually perform model binding
            Device device = JsonConvert.DeserializeObject<Device>(jsonString);

            return View("VulnerabilityResults", device);
        }

        [HttpPost]
        public ViewResult GetVulnerabilityMitigations(string jsonString)
        {
            // Manually perform model binding
            Vulnerability vulnerability = JsonConvert.DeserializeObject<Vulnerability>(jsonString);

            return View("MitigationResults", vulnerability);
        }

        #endregion

        #region Progress Updates

        [HttpPost]
        public ViewResult GetProgressUpdate()
        {
            return View();
        }

        #endregion

        #region Network Option

        [HttpPost]
        public ActionResult GetAvailableNetworks()
        {
            var availableNetworks = new List<string>();

            foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                // check interface is up
                if (networkInterface.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (UnicastIPAddressInformation addressInformation in networkInterface.GetIPProperties().UnicastAddresses)
                    {
                        // check address is IPv4
                        if (addressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            // check address isn't loopback
                            if (!IPAddress.IsLoopback(addressInformation.Address))
                            {
                                availableNetworks.Add(addressInformation.Address.ToString());
                            }
                        }
                    }
                }
            }
            
            // Include only unique entries
            availableNetworks = availableNetworks.Distinct().ToList();

            // Return JSON to the client so we can populate a dropdown list on UI
            return Json(availableNetworks);
        }

        #endregion
    }
}
