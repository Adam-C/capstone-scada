﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace scada_app.Controllers
{
    public class HomeController : Controller
    {
        // GET /home
        public IActionResult Index()
        {
            // Returns the main page (Views/Home/Index.cshtml)
            return View();
        }
    }
}
