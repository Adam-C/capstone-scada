﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace scada_app.Logic
{
    /// <summary>
    /// Responsible for handling information read during real-time update of scan progress.
    /// </summary>
    public static class ProgressUpdate
    {
        private static StreamWriter GetStreamWriter()
        {
            var fs = File.Open(Constants.ProgressUpdateFilePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
            return new StreamWriter(fs);
        }

        private static StreamReader GetReadableStream()
        {
            var fs = File.Open(Constants.ProgressUpdateFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return new StreamReader(fs);
        }

        public static void ClearFile()
        {
            var file = File.Open(Constants.ProgressUpdateFilePath, FileMode.Truncate, FileAccess.Write, FileShare.ReadWrite);
            file.Dispose();
        }

        public static void WriteActivityMessage(string message, bool withCurrentTime = false)
        {
            // write message to activity log
            Log.WriteMessage(message, Constants.ActivityLogFilePath, withCurrentTime);

            // write message to progress update
            StreamWriter writer = null;
            try
            {
                using (writer = GetStreamWriter())
                {
                    
                    if (withCurrentTime)
                    {
                        var nowTime = DateTime.Now;
                        writer.WriteLine(nowTime + ": " + message);
                    }
                    else
                    {
                        writer.WriteLine(message);
                    }
                }
            }
            finally
            {
                if (writer != null)
                    writer.Dispose();
            }
        }

        public static List<String> GetMessages()
        {
            var messages = new List<String>();

            StreamReader reader = null;
            try
            {
                using (reader = GetReadableStream())
                {
                    while (!reader.EndOfStream)
                    {
                        messages.Add(reader.ReadLine());
                    }
                }
            }
            finally
            {
                if (reader != null)
                    reader.Dispose();
            }
            
            return messages;
        }
    }
}
