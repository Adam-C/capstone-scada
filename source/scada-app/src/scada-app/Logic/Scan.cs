﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using scada_app.Logic.ProtocolTests;

namespace scada_app.Logic
{
    /// <summary>
    /// Responsible for scanning and retrieving info for single address.
    /// </summary>
    public static class Scan
    {
        public static void Perform(ActionData actionData)
        {

            ProgressUpdate.WriteActivityMessage("Attempting scan at " + actionData.ScanRequest.IpAddress);

            // Check if host is reachable with ping
            // Abort if IP address is not valid
            IPAddress address;
            if (!IPAddress.TryParse(actionData.ScanRequest.IpAddress, out address) ||
                address.AddressFamily != AddressFamily.InterNetwork) return;
            PingReply pingReply = null;
            try { pingReply = new Ping().SendPingAsync(address, Constants.TimeoutProtocolConnection).Result; }
            catch { return; }
            
            if (pingReply != null && pingReply.Status == IPStatus.Success)
            {
                ProgressUpdate.WriteActivityMessage("Host is reachable at " + actionData.ScanRequest.IpAddress);

                // Get supported protocols to iterate through from Constants
                var protocols = Constants.GetProtocols();

                // Iterate through protocols: Key = protocol name, Value = protocol port 
                foreach (var protocol in protocols)
                {
                    ProgressUpdate.WriteActivityMessage("Attempting " + protocol.Key + " connection at " +
                                                        actionData.ScanRequest.IpAddress);

                    if (TryConnection(actionData.ScanRequest.IpAddress, protocol.Value))
                    {
                        var currentDevice = new Device
                        {
                            Protocol = protocol.Key,
                            IpAddress = actionData.ScanRequest.IpAddress
                        };

                        actionData.ScanResult.Devices.Add(currentDevice);

                        ProgressUpdate.WriteActivityMessage("Found " + protocol.Key + " service at " + actionData.ScanRequest.IpAddress);

                        // Increment which device is current
                        // Devices are always added to end of list so we can iterate this way
                        actionData.ScanRequest.CurrentDeviceIndex += 1;

                        // Run all generic tests
                        VulnerabilityTest.RunAllGenericTests(actionData);

                        // Run protocol-specific tests
                        switch (protocol.Key)
                        {
                            case Constants.Protocols.Modbus.Name:
                                ProgressUpdate.WriteActivityMessage("Starting Modbus tests");
                                ModbusTest.RunAll(actionData);
                                break;
                            case Constants.Protocols.EthernetIP.Name:
                                ProgressUpdate.WriteActivityMessage("Starting EthernetIP tests");
                                EthernetIPTest.RunAll(actionData);
                                break;
                        }

                        // Search database and add relevant vulnerabilities and mitigations
                        LocalDb.LookupDatabaseVulnerabilities(actionData);
                        LocalDb.LookupDatabaseMitigations(actionData);

                        // TODO: remove break when we can confirm multiple protocols on a single address
                        break;
                    }
                }
            }

            //AddTestDataWithDatabaseLookup(actionData);

        }

        /// <summary>
        /// Creates a list of possible hosts in a /24 network (excluding 0, localIP and 255)
        /// </summary>
        /// <param name="localAddressString"> the IP address of the local machine </param>
        /// <returns> List of possible hosts in a /24 network (excluding 0, localIP and 255)</returns>
        public static List<string> GetPossibleHosts(string localAddressString)
        {
            var possibleHosts = new List<string>();
            var localAddress = new IPAddress(0);
            var subnetMask = new IPAddress(0);

            // set localIP and subnetMask
            foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation addressInformation in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (addressInformation.Address.ToString() == localAddressString)
                    {
                        localAddress = addressInformation.Address;
                        subnetMask = addressInformation.IPv4Mask;
                    }
                }
            }

            // get network address
            var networkAddress = IPAddressExtensions.GetNetworkAddress(localAddress, subnetMask);

            //get broadcast address
            var broadcastAddress = IPAddressExtensions.GetBroadcastAddress(localAddress, subnetMask);

            // convert network address to uint
            var networkAddressUInt = ByteUtilities.BytesToUInt(networkAddress.GetAddressBytes());

            // convert localIp to int
            var localAddressUInt = ByteUtilities.BytesToUInt(localAddress.GetAddressBytes());

            // convert broadcastAddress to int
            var broadcastAddressUInt = ByteUtilities.BytesToUInt(broadcastAddress.GetAddressBytes());

            // add all possibleHosts that aren't localAdd
            for (var i = networkAddressUInt + 1; i < broadcastAddressUInt; i++)
            {
                if (i != localAddressUInt)
                {
                    var currentAddress = ByteUtilities.UIntToBytes(i, 4);
                    possibleHosts.Add(currentAddress[0] + "." + currentAddress[1] + "." + currentAddress[2] + "." + currentAddress[3]);
                }
            }
            return possibleHosts;
        }
        
        /// <summary>
        /// Attempts TCP connection to device with specified address.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        /// <returns>True if connection was successful, false otherwise</returns>
        private static bool TryConnection(string address, int port)
        {
            TcpClient tcpClient = new TcpClient();

            try
            {
                // Try and get a response before timing out
                if (tcpClient.ConnectAsync(address, port).Wait(Constants.TimeoutProtocolConnection))
                {
                    return true;
                }
                else
                {
                    // Didn't connect before timing out
                    return false;
                }

            } catch {
                return false;
            }
        }

        /// <summary>
        /// TEST method to add fake devices and perform real database vulnerability search.
        /// Must ensure there is db data for this data to simulate real database lookups.
        /// </summary>
        public static void AddTestDataWithDatabaseLookup(ActionData actionData)
        {
            actionData.ScanResult.Devices.Add(new Device
            {
                Description = "National Instruments 92837",
                IpAddress = "10.10.10.11",
                Manufacturer = "National Instruments",
                Model = "439-d",
                Protocol = Constants.Protocols.Modbus.Name
            });

            // Increment current device
            actionData.ScanRequest.CurrentDeviceIndex += 1;

            actionData.GetCurrentDevice().Vulnerabilities.Add(new Vulnerability {
                ManuallyDetected = true,
                Description = "this vuln was found on the device manually"
            });

            LocalDb.LookupDatabaseVulnerabilities(actionData);
            LocalDb.LookupDatabaseMitigations(actionData);

            actionData.ScanResult.Devices.Add(new Device
            {
                Description = "Allen Bradley 3000",
                IpAddress = "10.10.10.99",
                Manufacturer = "Allen Bradley",
                Model = "3000",
                Protocol = Constants.Protocols.EthernetIP.Name,
                SerialNumber = 999,
                ItemType = 400,
                ProductCode = 20,
                RevisionNumber = "4.88"
            });

            actionData.ScanRequest.CurrentDeviceIndex += 1;

            LocalDb.LookupDatabaseVulnerabilities(actionData);
            LocalDb.LookupDatabaseMitigations(actionData);

        }

    }
}
