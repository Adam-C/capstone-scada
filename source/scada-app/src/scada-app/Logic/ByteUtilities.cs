﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace scada_app.Logic
{
    public class ByteUtilities
    {
        /// <summary>
        /// Converts an integer to a big or little-endian byte array.
        /// intValue must be less than 2 ^ (byteAmount * 8).
        /// intValue must be greater than 0.
        /// </summary>
        /// <param name="intValue"> the integer value to be converted </param>
        /// <param name="byteAmount"> the amount of bytes in the returned array </param>
        /// <param name="isBigEndian"> true if the array is to be big-endian, otherwise false </param>
        /// <returns> An array containing the byte[] value of the specified integer </returns>
        public static byte[] UIntToBytes(uint intValue, int byteAmount, bool isBigEndian = true)
        {
            const int bitsInByte = 8;
            var intBytes = new byte[byteAmount];
            if (isBigEndian)
            {
                for (int i = 0; i < byteAmount; i++)
                {
                    intBytes[i] = (byte)(intValue >> ((byteAmount - 1 - i) * bitsInByte));
                }
            }
            else
            {
                for (int i = byteAmount - 1; i >= 0; i--)
                {
                    intBytes[i] = (byte)(intValue >> ((i) * bitsInByte));
                }
            }
            return intBytes;
        }

        /// <summary>
        /// Converts a byte array to an unsigned integer
        /// </summary>
        /// <param name="byteValue"></param>
        /// <returns></returns>
        public static uint BytesToUInt(byte[] byteValue)
        {
            var byteCopy = new byte[byteValue.Length + 4];
            Array.Copy(byteValue, 0, byteCopy, byteCopy.Length - byteValue.Length, byteValue.Length);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(byteCopy);
                return BitConverter.ToUInt32(byteCopy, 0);
            }
            return BitConverter.ToUInt32(byteCopy, byteCopy.Length - 4);
        }
    }
}
