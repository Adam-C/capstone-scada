﻿using System.Net.Sockets;
using EIPNET.EIP;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Net.Http;

namespace scada_app.Logic.ProtocolTests
{
    public static class EthernetIPTest
    {
        /// <summary>
        /// Runs all implemented tests for EthernetIp devices
        /// </summary>
        /// <param name="actionData"></param>
        public static void RunAll(ActionData actionData)
        {
            //create and register a session with the IP address stored in actionData
            ProgressUpdate.WriteActivityMessage("attempting EthernetIP connection to device at " +
               actionData.ScanRequest.IpAddress);
            var sessionInfo = SessionManager.CreateAndRegister(actionData.ScanRequest.IpAddress);

            //end tests if an initial EIP connection cannot be made
            if (!sessionInfo.Connected)
            {
               return;
            }

            // if the device doesn't support CIP, end the session
            if (!SessionManager.VerifyCIP(sessionInfo)) {
               SessionManager.UnRegister(sessionInfo);
               return;
            }
            
            //record device identity information in actionData
            ProgressUpdate.WriteActivityMessage("Querying and setting device identity information");
            setDeviceInformation(actionData, sessionInfo);

            //end the session
            SessionManager.UnRegister(sessionInfo);
        }

        /// <summary>
        /// Set the information obtained from VerifyCIP to the actionData object
        /// </summary>
        /// <param name="actionData"></param>
        /// <param name="sessionInfo"></param>
        public static void setDeviceInformation(ActionData actionData, SessionInfo sessionInfo)
        {
            // set the device model
            actionData.GetCurrentDevice().Model = 
               sessionInfo.IdentityInfo.ProductName.ToString();

            // set the device manufacturer
            actionData.GetCurrentDevice().Manufacturer =
               getManufacturerString(sessionInfo.IdentityInfo.VendorId);

            // set the serial number
            actionData.GetCurrentDevice().SerialNumber =
               sessionInfo.IdentityInfo.SerialNumber;

            //set the revision number
            actionData.GetCurrentDevice().RevisionNumber =
               getRevisionString(sessionInfo.IdentityInfo.Revision);

            //set the product code
            actionData.GetCurrentDevice().ProductCode =
               sessionInfo.IdentityInfo.ProductCode;

            //set the item type
            actionData.GetCurrentDevice().ItemType =
               sessionInfo.IdentityInfo.ItemTypeCode;
        }

        /// <summary>
        /// Utility to get "manufacturer" string for use in actionData objects
        /// </summary>
        /// <param name="vendorID"></param>
        /// <returns>String to be dislayed to used containing manufacturer information</returns>
        private static string getManufacturerString(ushort vendorID)
        {
            //get the list of Vendor IDs
            var vendorIDs = Constants.GetEIPVendorIDs();
            
            // check if our vendorID matches known vendors
            foreach (var vendor in vendorIDs)
            {
                if (vendor.Key == vendorID)
                {
                    return vendor.Value;
                }
            }
            // no current VendorID data exsists, return default
            return "ODVA Vendor ID #: " + vendorID;
        }


        private static string getRevisionString (byte[] revision)
        {
            return revision[0] + "." + revision[1];
        }


        /// <summary>
        /// attempts to send a CIP forward open packet to a device to create a session
        /// currently hard-coded for the device in the lab based on captured comms with the HMI
        /// Included to provide insight for future teams
        /// </summary>
        /// <param name="sessionInfo"> session information </param>
        private static void beginForwardOpen (SessionInfo sessionInfo)
        {
           // set CIP connection parameters - based on packets captured from HMI
           byte[] path = { 0x01, 0x00, 0x20, 0x02, 0x24, 0x01 };
           short O2Tparam = 0x43fc;
           ushort ConnSerialNum = 0x002e;
           uint CIPTimeout = 195;
           uint T2OconnID = 0x0000002d;
           ushort vendorID = 0x5241;
           uint OserialNum = 0x45494843;
           sessionInfo.SetConnectionParameters(ConnSerialNum, CIPTimeout, T2OconnID, vendorID, OserialNum);

           // Forward Open request - CONFIRMED WORKING
           EIPNET.ConnectionManager.ForwardOpen(sessionInfo, path, O2Tparam);
        }

        /// <summary>
        /// Scrapes text from a webserver hosted at the address of the device
        /// Sets firmware version on actionData object if information is available
        /// </summary>
        /// <param name="actionData"></param>
        public static void ScrapeWebServer(ActionData actionData)
        {
           // Get current device
           var device = actionData.GetCurrentDevice();

           // Get raw HTML
           string page = "/home.asp";
           var requestClient = new HttpClient();

           requestClient.BaseAddress = new System.Uri(device.IpAddress + page);
           var responseString = requestClient.GetAsync(string.Empty).Result.Content.ReadAsStringAsync().Result;

           HtmlDocument doc = new HtmlDocument();
           doc.LoadHtml(responseString);

           // Select rows from table
           var rows = doc.DocumentNode.SelectNodes("//table//tr");
            
           if (rows != null && rows.Count > 0)
           {
               foreach (var row in rows)
               {
                   var cells = row.SelectNodes(".//td");
                   var key = cells[0].InnerText.Trim();

                   if (key == "Firmware Version Date")
                   {
                       device.FirmwareVersion = cells[1].InnerText.Trim();
                   }

                   // TODO: add more fields if necessary
                   //} else if (key == "Serial Number") {

                   //    device.SerialNumber = cells[1].InnerText.Trim();
                   //}
               }
           }
        }
    }
}
