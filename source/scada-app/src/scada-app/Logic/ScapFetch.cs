﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace scada_app.Logic
{
    [Obsolete("SCAPSync is not currently operational.")]
    public static class ScapFetch
    {
        const string HostAddress = "https://scapsync.com";
        const int RequestTimeout = 20000;

        class Endpoints
        {
            public const string Search = "/search?q=";
            public const string Cve = "/cve/";
        }

        public static dynamic SearchByKeyword(string keyword)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(HostAddress + Endpoints.Search + keyword);

            var responseString = client.GetAsync(string.Empty).Result.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<dynamic>(responseString);
        }
    }
}
