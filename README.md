# SCADA Vulnerability Scanner
##### Group 18 - IT Capstone - CAB398/399
An application to identify and suggest mitigations for security vulnerabilities on your SCADA network.

## Getting started
In the **releases** folder you can find a native version for your system. To use the app, simply run the **startup.bat** inside.

Alternatively, **scada-app** and **scada-server** can be run by installing [.NET Core](https://www.microsoft.com/net/core) on your system and using the **dotnet run** command in the project src directory. This method supports all platforms supported by .NET Core.

## Components

### scada-app
Supports Windows (7 and onwards), Linux, Mac.
The client app is responsible for scanning the local network for SCADA devices and performing vulnerability tests. It combines this with information from the database and displays mitigations to the user.

- ASP.NET Core
    - .NET Core (multi-platform)
    - and .NET Framework 4.6 (Windows only)
- EIPNET
- SnmpSharpNet
- jQuery
- Bootstrap

### scada-server
Supports Windows (7 and onwards), Linux, Mac.
The server app is responsible for responding to any client requests for the latest database (served as an SQLite .db file). It also regularly updates its vulnerability information from external sources.
Tested with Ubuntu 16.04 with Nginx on Microsoft Azure.

- ASP.NET Core
    - .NET Core (multi-platform)
- Microsoft.Data.Sqlite
- HtmlAgilityPack.NetCore

### scada-xp
This is a .NET 4.0 version of scada-app, for Windows XP support. It is not 100% stable and the main scada-app should be used instead wherever possible.

## More info

This product contains code from Ingenious LLC's EIPNET Library. The source code may be available from http://eipnet.codeplex.com . Ingenious LLC http://www.ingeniousllc.co .