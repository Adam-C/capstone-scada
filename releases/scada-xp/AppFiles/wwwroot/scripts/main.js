$(document).ready(function () {

    // Globals -------------------------------------

    var scanInProgress = false;

    // DOM shortcuts -------------------------------

    var $Templates = $('#Templates');

    var $HeaderText = $('#HeaderText');

    var $Menu = $('#Menu');
    var $NetworkDropdownContainer = $('#NetworkDropdownContainer');
    var $Updates = $('#Updates');

    var $Devices = $('#Devices');
    var $Vulns = $('#Vulnerabilities');
    var $Mitigations = $('#Mitigations');

    var $Popup = $('#Popup');

    // =========== DOM helpers ========================================

    function showLoadingGif() {
        disableMenu();
        $Updates.empty();

        // Clone the loading template so we can use it
        var loading = $Templates.find("#LoadingTemplate").clone();
        $(loading).attr('id', 'Loading');
        $Updates.append(loading);
    }

    function hideLoadingGif() {
        $Updates.find('#Loading').remove();
        enableMenu();
    }

    function enableMenu() {
        $Menu.find('button').prop('disabled', false);
        $Menu.find('input').prop('disabled', false);
    }

    function disableMenu() {
        $Menu.find('button').prop('disabled', true);
        $Menu.find('input').prop('disabled', true);
    }

    // ---------- Update message helpers ---------------------------
    function clearAndWriteUpdateMessage(message) {
        $Updates.html('<p>' + message + '</p>');
        keepUpdatesScrollAtTail();
    }

    function appendUpdateMessage(message) {
        $Updates.append('<p>' + message + '</p>');
        keepUpdatesScrollAtTail();
    }

    function keepUpdatesScrollAtTail() {
        if ($Updates.length === 1) {
            $Updates[0].scrollTop = $Updates[0].scrollHeight;
        }
    }

    // ---------- Misc. helpers ------------------------------------

    // Retrieve and display vulnerabilities of first device if it exists
    function autoRetrieveFirstDeviceVulns() {

        var deviceJsonString = $Devices.find('.deviceEntry');
        var jsonString;
        if (deviceJsonString.length !== 0) {
            jsonString = ($(deviceJsonString).eq(0).data('device'));

            $.ajax('/Logic/GetDeviceVulnerabilities',
            {
                method: "POST",
                data: jsonString,
                success: function (data) {

                    $Vulns.html(data);
                    hideLoadingGif();

                    // Show first device as selected
                    $Devices.find('.deviceEntry').eq(0).addClass('selectedEntry');
                }
            });

        } else {
            // No device
            hideLoadingGif();
        }

    }

    function makeResultsContentScrollable() {
        var resultsSections = $('#ResultsSections .resultSectionContent');
        if (resultsSections.length > 0) {
            resultsSections.height($(window).height() - resultsSections.offset().top);
        }
    }

    // -------- Startup helpers ------------------------------------

    function resetUi() {
        makeResultsContentScrollable();
    }

    function setupDb() {

        disableMenu();

        clearAndWriteUpdateMessage('Checking for local database updates');

        $.ajax('/Logic/SetupDb',
        {
            method: "POST",
            success: function (data) {

                // Display result of database setup
                $Updates.html(data);

            }
        });
    }

    function getAvailableNetworks() {

        $.ajax('/Logic/GetAvailableNetworks', {
            method: 'POST',
            success: function (data) {

                var $dropdown = $NetworkDropdownContainer.find('.dropdown-menu');

                for (var i = 0; i < data.length; i++) {

                    // Create and add list item
                    var li = '<li><a>';
                    li += data[i];
                    li += '</a></li>';
                    $dropdown.append(li);
                }

                // Select network if only one
                var $listItems = $dropdown.find('li');
                if ($listItems.length === 1) {
                    $NetworkDropdownContainer.data('selected-network', $listItems.eq(0).text());
                    $listItems.eq(0).trigger('click');
                }

                enableMenu();
            }
        })

    }

    // ==================== App initialisation ==========================

    resetUi();

    setupDb();

    getAvailableNetworks();

    // ================= Event handlers ==================================

    // Events where UI needs to be reset
    $(document).ajaxSuccess(function(e) {
        resetUi();
    });

    $(window).on('resize', function(e) {
        resetUi();
    });

    // Alert before exiting
    $(window).on('beforeunload', function(e) {
        if (scanInProgress) {
            var message = "There is a scan in progress. Are you sure you wish to abort?";
            e.returnValue = message;
            return message;
        }
    });

    // Abort scan
    $(window).on('unload', function(e) {
        if (scanInProgress) {
            $.ajax('/Logic/AbortScan',
                {
                    method: 'POST',
                    async: false
                });
        }
    });

    // -------------- Menu -------------------------------------

    // Network dropdown
    $NetworkDropdownContainer.delegate('li', 'click', function (e) {

        var network = $(e.currentTarget).text();
        $NetworkDropdownContainer.data('selected-network', network);
        $('#NetworkDropdownMenu span.content').text(network);
    });

    // Enter key pressed in single address input
    $Menu.delegate('#AddressInput', 'keyup', function (e) {

        // Click button if Enter pressed
        if (e.key === 'Enter') {
            $Menu.find('#ScanSingleAddressButton').trigger('click');
        }
    });

    // Begin 'Scan single address'
    $Menu.delegate('#ScanSingleAddressButton', 'click', function(e) {

        var errorMessage;
        
        // Validate IP address
        var address = $('#AddressInput').val();

        // Terminate if invalid input
        if (address === '' || address.match(/[a-z]/i)) {

            var msg = '<div class="errorMessage"><h4>Please enter a valid address.</h4></div>';

            errorMessage = $Updates.find('.errorMessage');
            if (errorMessage.length === 0) {
                $Updates.append(msg);
            }

            return;

        } else {
            // Remove error message if input is valid
            errorMessage = $Menu.find('.errorMessage');
            if (errorMessage.length !== 0) {
                errorMessage.remove();
            }
        }

        var continueUpdates = true;
        var updateTime = 100; // How often to poll for progress updates (in ms)

        showLoadingGif();
        scanInProgress = true;
        // Send request to LogicController
        $.ajax('/logic/ScanSingleAddress',
        {
            method: "GET",
            // The "ipAddress" property name matches the parameter name in the controller method
            data: { ipAddress: address },
            success: function (data) {

                scanInProgress = false;

                // Stop progress updates
                continueUpdates = false;

                // Show devices on display
                $Devices.html(data);

                // Retrieve vulnerabilities of first device if it exists
                autoRetrieveFirstDeviceVulns();

                appendUpdateMessage('Scan completed');

                enableMenu();
            }
        });

        // Begin continuous requests for progress update
        getProgressUpdate();

        function getProgressUpdate() {

            // Wait appropriate amount of time before polling
            setTimeout(function() {
                    $.ajax('/logic/GetProgressUpdate',
                    {
                        method: "POST",
                        success: function(data) {

                            if (continueUpdates) {

                                // Update progress display
                                clearAndWriteUpdateMessage(data);

                                keepUpdatesScrollAtTail();

                                // Call self to repeat
                                getProgressUpdate();
                            }
                        }
                    });
                },
                updateTime);
        }

    });

    // Begin 'Scan entire network'
    $Menu.delegate('#ScanEntireNetworkButton', 'click', function(e) {

        var continueUpdates = true;
        var updateTime = 100; // How often to poll for progress updates (in ms)

        var network = $NetworkDropdownContainer.data('selected-network');

        showLoadingGif();
        scanInProgress = true;

        $.ajax('/logic/ScanEntireNetwork',
        {
            method: "GET",
            data: {localIp: network},
            success: function (data) {

                continueUpdates = false;

                scanInProgress = false;

                // Replace Content div with rendered view
                $Devices.html(data);

                // Retrieve vulnerabilities of first device if it exists
                autoRetrieveFirstDeviceVulns();

                appendUpdateMessage('Scan completed');

                enableMenu();

            }
        });

        // Begin continuous requests for progress update
        getProgressUpdate();

        function getProgressUpdate() {

            // Wait appropriate amount of time before polling
            setTimeout(function () {
                $.ajax('/logic/GetProgressUpdate',
                {
                    method: "POST",
                    success: function (data) {

                        if (continueUpdates) {

                            // Update progress display
                            clearAndWriteUpdateMessage(data);

                            keepUpdatesScrollAtTail();

                            // Call self to repeat
                            getProgressUpdate();
                        }
                    }
                });
            },
            updateTime);
        }

    });

    // -------------- Device Results ------------------------------

    // Click device to display vulnerabilities
    $Devices.delegate('.deviceEntry', 'click', function(e) {

        var deviceJsonString = $(e.currentTarget).data('device');

        showLoadingGif();
        $.ajax('/Logic/GetDeviceVulnerabilities',
            {
                method: "POST",
                data: deviceJsonString,
                success: function (data) {

                    $Vulns.html(data);
                    hideLoadingGif();

                    // Show device as selected
                    $('.deviceEntry').removeClass('selectedEntry');
                    $(e.currentTarget).addClass('selectedEntry');

                    $('#Mitigations .resultSectionContent').empty();
                }
            });
    });

    // Click device for more info popup dialog
    $Devices.delegate('.deviceEntry .moreInfoPopupButton', 'click', function(e) {

        // Prevent being caught by other attached events
        e.stopPropagation();

        // Get relevant content
        var content = $(e.currentTarget).siblings('.moreInfoPopupContent').eq(0).html().trim();

        // Place content in popup template
        $Popup.html(content);

        // Show popup
        $Popup.dialog({
            width: 'auto'
        });

    });

    // ----------- Vulnerability Results ---------------------------

    // Click vulnerability to display mitigations
    $Vulns.delegate('.vulnEntry', 'click', function(e) {

        var originalTarget = $(e.originalEvent.srcElement);
        if (originalTarget.is('button.reference-list') || originalTarget.parents('button.reference-list').length === 1 ||
            originalTarget.parents('.dropdown > .dropdown-menu') === 1) {
            return;
        }

        var vulnJsonString = $(e.currentTarget).data('vulnerability');

        showLoadingGif();
        $.ajax('/Logic/GetVulnerabilityMitigations',
        {
            method: "POST",
            data: vulnJsonString,
            success: function(data) {

                $Mitigations.html(data);
                hideLoadingGif();

                // Highlight URLs in mitigation text
                var mitigations = $('.mitigationEntry > p');
                var mitigationsText = mitigations.html();
                // var urlMatch = /(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/g;
                var urlMatch = /(http|https|ftp|ftps)\:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+~#?&//=]*)/g;

                if (mitigationsText) {
                    mitigationsText = mitigationsText.replace(urlMatch, function(match) {
                        return '<a target="_blank" rel="noopener noreferrer" href="' + match + '">' + match + '</a>';
                    }); 
                    mitigations.html(mitigationsText);
                }

                // Show vulnerability as selected
                $('.vulnEntry').removeClass('selectedEntry');
                $(e.currentTarget).addClass('selectedEntry');

            }
        });

    });

    $('document').delegate('button.dropdown-toggle', 'click', function(e) {
        
        e.stopPropagation();
    });

});